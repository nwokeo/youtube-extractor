from apiclient.discovery import build
import boto3
import configparser
from datetime import datetime
import json
import logging
import os


class YoutubeExtract:
    def __init__(self):
        self.log = logging.getLogger(__name__)
        self.log.setLevel(logging.DEBUG)

        config = configparser.ConfigParser()
        config.read('config.cfg')

        self.developer_key = config.get('youtube', 'developer_key')
        self.youtube_api_service_name = config.get('youtube', 'api_service_name')
        self.youtube_api_version = config.get('youtube', 'api_version')
        self.bucket_name = config.get('aws', 'bucket_name')
        self.aws_access_key_id = config.get('aws', 'access_key_id')
        self.aws_secret_access_key = config.get('aws', 'secret_access_key')
        self.data_dir = 'data/'
        self.client = self.build_yt_client()

    def get_videos_by_channel(self, channel_id):
        response = self.client.search().list(
            channelId=channel_id,
            type='video',
            part='id,snippet',
            maxResults=50
        ).execute()

        video_ids = []

        for video in response['items']:
            video_ids.append(video['id']['videoId'])

        return ','.join(video_ids)

    def get_channel_stats(self, channel_ids):
        response = self.client.channels().list(
            id=channel_ids,
            part='id, snippet, statistics, status'
        ).execute()

        extracted_at = datetime.utcnow()
        for channel in response['items']:
            channel['extracted_at'] = str(extracted_at)
            filename = self.data_dir + 'channel/' + channel['id'] + '_' + extracted_at.strftime(
                '%Y%m%d_%H%M%S_%f') + '.json'
            with open(filename, 'w') as f:
                json.dump(channel, f)

    def get_video_stats(self, video_ids):
        response = self.client.videos().list(
            id=video_ids,
            part='id, snippet, contentDetails, statistics, status'
        ).execute()

        extracted_at = datetime.utcnow()
        for video in response['items']:
            video['extracted_at'] = str(extracted_at)
            filename = self.data_dir + 'video/' + video['id'] + '_' + extracted_at.strftime(
                '%Y%m%d_%H%M%S_%f') + '.json'
            with open(filename, 'w') as f:
                json.dump(video, f)

    def upload_to_s3(self):
        s3 = boto3.resource('s3',
                            aws_access_key_id=self.aws_access_key_id,
                            aws_secret_access_key=self.aws_secret_access_key)

        for root, dirnames, filenames in os.walk('data'):
            for filename in filenames:
                filepath = os.path.join(root, filename)
                s3.meta.client.upload_file(filepath, self.bucket_name, filepath)
                os.remove(filepath)

    def build_yt_client(self):
        return build(
            self.youtube_api_service_name,
            self.youtube_api_version,
            developerKey=self.developer_key)
