import youtube_extract
from apiclient.errors import HttpError

yt = youtube_extract.YoutubeExtract()

channel_ids = ['UCJS9pqu9BzkAMNTmzNMNhvg',
               'UCOjD18EJYcsBog4IozkF_7w',
               'UCG08EqOAXJk_YXPDsAvReSg',
               'UCAezwIIm1SfsqdmbQI-65pA',
               'UCN2nbMPLJWv3Y__4JuF_hMQ']
try:
    yt.get_channel_stats(','.join(channel_ids))

    for channel_id in channel_ids:
        video_ids = yt.get_videos_by_channel(channel_id)
        yt.get_video_stats(video_ids)

    yt.upload_to_s3()

except HttpError as e:
    print('error:', e)
