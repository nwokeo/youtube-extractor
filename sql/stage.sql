truncate table staging.youtube_channel_json;
COPY staging.youtube_channel_json
FROM 's3://{bucket_name}/data/channel/'
access_key_id ''
secret_access_key ''
fixedwidth '65535';

truncate table staging.youtube_channel_json;
COPY staging.youtube_channel_json
FROM 's3://{bucket_name}/data/video/'
access_key_id ''
secret_access_key ''
fixedwidth '65535';