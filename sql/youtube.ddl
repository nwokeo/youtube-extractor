create table staging.youtube_channel_json(
  raw_json varchar(max)
);

create table staging.youtube_video_json(
  raw_json varchar(max)
);

create table public.youtube_channel(
  channel_key int IDENTITY,
  channel_id varchar(1024) distkey,
  channel_name varchar(1024),
  subscriber_count bigint,
  extract_time timestamp,
  insert_time timestamp default sysdate
)
diststyle key;

create table public.youtube_video(
  video_key int IDENTITY,
  video_id varchar(1024),
  channel_id varchar(1024) distkey,
  video_title varchar(1024),
  video_description varchar(5000),
  publish_date timestamp,
  view_count bigint,
  like_count bigint,
  dislike_count bigint,
  comment_count bigint,
  channel_subscriber_count bigint,
  extract_time timestamp,
  insert_time timestamp default sysdate
)
diststyle key;

create table public.video_aggregates(
  video_id varchar(255),

  view_count_1d bigint,
  like_count_1d bigint,
  dislike_count_1d bigint,
  comment_count_1d bigint,
  channel_subscriber_count_1d bigint,

  view_count_7d bigint,
  like_count_7d bigint,
  dislike_count_7d bigint,
  comment_count_7d bigint,
  channel_subscriber_count_7d bigint,

  view_count_28d bigint,
  like_count_28d bigint,
  dislike_count_28d bigint ,
  comment_count_28d bigint,
  channel_subscriber_count_28d bigint,

  view_count_lifetime bigint,
  like_count_lifetime bigint,
  dislike_count_lifetime bigint,
  comment_count_lifetime bigint,
  channel_subscriber_count_lifetime bigint
);

-- optional: query raw data w/ athena
CREATE EXTERNAL TABLE IF NOT EXISTS default.youtube (
  `id` string,
  statistics struct<
              `subscriberCount`:string,
              `videoCount`:string,
              `commentCount`:string,
              `viewCount`:string
              >
  )
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION 's3://nwoke-youtube/';