--publish channel stats
truncate table public.youtube_channel;
insert into public.youtube_channel (
  channel_id,
  channel_name,
  subscriber_count,
  extract_time
)
select distinct
  json_extract_path_text(raw_json, 'id') as channel_id,
  json_extract_path_text(raw_json, 'snippet', 'title') as channel_name,
  json_extract_path_text(raw_json, 'statistics', 'subscriberCount')::bigint as subscriber_count,
  json_extract_path_text(raw_json, 'extracted_at')::timestamp  as extract_time
from staging.youtube_channel_json stg
left join public.youtube_channel yc
  on json_extract_path_text(stg.raw_json, 'id') = yc.channel_id
  and json_extract_path_text(stg.raw_json, 'extracted_at')::timestamp = yc.extract_time
where
  yc.channel_key is null
;

--publish video stats
truncate table public.youtube_video;
insert into public.youtube_video(
  video_id,
  channel_id,
  video_title,
  video_description,
  publish_date,
  view_count,
  like_count,
  dislike_count,
  comment_count,
  channel_subscriber_count,
  extract_time
)
select
  json_extract_path_text(raw_json, 'id') as video_id,
  json_extract_path_text(raw_json, 'snippet', 'channelId') as channel_id,
  json_extract_path_text(raw_json, 'snippet', 'title') as video_title,
  json_extract_path_text(raw_json, 'snippet', 'description') as video_description,
  json_extract_path_text(raw_json, 'snippet', 'publishedAt')::timestamp as publish_date,
  nullif(json_extract_path_text(raw_json, 'statistics', 'viewCount'), '')::bigint as view_count,
  nullif(json_extract_path_text(raw_json, 'statistics', 'likeCount'), '')::bigint as like_count,
  nullif(json_extract_path_text(raw_json, 'statistics', 'dislikeCount'), '')::bigint as dislike_count,
  nullif(json_extract_path_text(raw_json, 'statistics', 'commentCount'), '')::bigint as comment_count,
  yc.subscriber_count as channel_subscriber_count,
  json_extract_path_text(raw_json, 'extracted_at')::timestamp as extract_time
from staging.youtube_video_json stg
left join public.youtube_channel yc
  on json_extract_path_text(stg.raw_json, 'snippet', 'channelId') = yc.channel_id
  and json_extract_path_text(stg.raw_json, 'extracted_at')::date = yc.extract_time::date
left join public.youtube_video yv
  on json_extract_path_text(stg.raw_json, 'id') = yv.video_id
  and json_extract_path_text(stg.raw_json, 'extracted_at')::timestamp = yv.extract_time
where
  yv.video_key is null
;

--publish aggregates
--assume inserts happen once and only once a day
TRUNCATE table public.video_aggregates;
insert into public.video_aggregates(
  video_id,
  view_count_1d, like_count_1d, dislike_count_1d, comment_count_1d,channel_subscriber_count_1d,
  view_count_7d, like_count_7d, dislike_count_7d, comment_count_7d,channel_subscriber_count_7d,
  view_count_28d, like_count_28d, dislike_count_28d, comment_count_28d,channel_subscriber_count_28d,
  view_count_lifetime, like_count_lifetime, dislike_count_lifetime, comment_count_lifetime,channel_subscriber_count_lifetime
)
select
  yv.video_id,

  yv.view_count - yv1.view_count as view_count_1d,
  yv.like_count - yv1.like_count as like_count_1d,
  yv.dislike_count - yv1.dislike_count as dislike_count_1d,
  yv.comment_count - yv1.comment_count as comment_count_1d,
  yv.channel_subscriber_count - yv1.channel_subscriber_count as channel_subscriber_count_1d,

  yv.view_count - yv7.view_count as view_count_7d,
  yv.like_count - yv7.like_count as like_count_7d,
  yv.dislike_count - yv7.dislike_count as dislike_count_7d,
  yv.comment_count - yv7.comment_count as comment_count_7d,
  yv.channel_subscriber_count - yv7.channel_subscriber_count as channel_subscriber_count_7d,

  yv.view_count - yv28.view_count as view_count_28d,
  yv.like_count - yv28.like_count as like_count_28d,
  yv.dislike_count - yv28.dislike_count as dislike_count_28d,
  yv.comment_count - yv28.comment_count as comment_count_28d,
  yv.channel_subscriber_count - yv28.channel_subscriber_count as channel_subscriber_count_28d,

  yv.view_count as view_count_lifetime,
  yv.like_count as like_count_lifetime,
  yv.dislike_count as dislike_count_lifetime,
  yv.comment_count as comment_count_lifetime,
  yv.channel_subscriber_count as channel_subscriber_count_lifetime

from youtube_video yv
left join youtube_video yv1
  on yv.video_id = yv1.video_id
  and yv1.extract_time::date = (sysdate - 1)::date
left join youtube_video yv7
  on yv.video_id = yv7.video_id
  and yv7.extract_time::date = (sysdate - 7)::date
left join youtube_video yv28
  on yv.video_id = yv28.video_id
  and yv28.extract_time::date = (sysdate - 28)::date
where
  yv.insert_time::date = sysdate::date
;