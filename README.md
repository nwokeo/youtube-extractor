# Extract Data From YouTube Videos
`youtube_daily_run.py` defines a list of channels and calls the `youtube_extract.py` module to retrieve channel and video details.

### Install
Only tested on ubuntu 16.04, python 3.4.3

`pip install -r requirements.txt`

Define AWS, Google credentials in:

`cp config.cfg.example config.cfg`


### Run

`python youtube_daily_run.py`

`python youtube_load_tables.py`


###  Description
`youtube_daily_run.py`
This file triggers the extract process. It’s intended to be launched by a scheduler and accept input (in this case, youtube channel IDs) from an external source.

`youtube_extract.py`
The extraction module writes raw data to s3 since it’d be nice to keep as much data as possible in raw format. This module can serve as an abstraction for any logic that may be required in the future.

Since youtube doesn’t provide a field for extract time, a timestamp is appended when writing the file.
 
`youtube_load_tables.py`
This is a wrapper script for the following SQL files. It should be called after the success of the extract process. For now, the actual execution logic is unimplemented - ideally this will be cross-database compatible.

`sql/stage.sql`
Load the raw data to staging tables in Redshift to process before publishing. 

`sql/publish.sql`
Publish previously un-inserted rows to an aggregate fact table. For now, assume here that this process can run only once per day.

